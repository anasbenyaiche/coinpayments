package main

import (
	"os"

	"gitlab.com/tokend/coinpayments/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
