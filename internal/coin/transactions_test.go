package coin

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnector_CreateTx(t *testing.T) {
	t.Run("good response", func(t *testing.T) {
		ts := prepareTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		tx := Transaction{
			Address:           &address,
			ReceivingCurrency: "BTC",
			SendingCurrency:   "BTC",
			Amount:            "1.00003212",
			BuyerEmail:        "vasil@vmail.com",
			ItemName:          &itemName,
			ItemNumber:        &itemNumber,
		}

		details, err := cc.CreateTx(tx)
		assert.NoError(t, err)
		assert.NotNil(t, details)
		assert.Equal(t, tx.Amount, details.Amount)
		assert.Equal(t, *tx.Address, details.Address)
	})

	t.Run("error response", func(t *testing.T) {
		ts := prepareFaultyTestServer()
		defer ts.Close()

		cc := Connector{
			config: Config{Address: ts.URL, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
			client: http.DefaultClient,
		}

		tx := Transaction{
			Address:           &address,
			ReceivingCurrency: "BTC",
			SendingCurrency:   "BTC",
			Amount:            "1.00003212",
			BuyerEmail:        "vasil@vmail.com",
			ItemName:          &itemName,
			ItemNumber:        &itemNumber,
		}

		details, err := cc.CreateTx(tx)
		assert.Error(t, err)
		assert.Nil(t, details)
	})
}
