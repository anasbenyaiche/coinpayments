package coin

import (
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type Coiner interface {
	Coin() *Connector
}

type coiner struct {
	getter kv.Getter
	once   comfig.Once
}

func NewCoiner(getter kv.Getter) Coiner {
	return &coiner{
		getter: getter,
	}
}

func (c *coiner) Coin() *Connector {
	return c.once.Do(func() interface{} {
		var config Config

		config, err := NewConfig(c.getter)
		if err != nil {
			panic(errors.Wrap(err, "failed to initialize coinpayments config"))
		}

		return NewConnector(config)
	}).(*Connector)
}
