package coin

import (
	"encoding/json"
	"net/url"

	"github.com/pkg/errors"
)

func composeURLValues(values map[string]string) url.Values {
	data := url.Values{}

	for k, v := range values {
		data.Set(k, v)
	}

	return data
}

func convertToMap(v interface{}) (map[string]string, error) {
	values := make(map[string]string)
	bb, err := json.Marshal(v)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal request")
	}

	err = json.Unmarshal(bb, &values)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal request")
	}
	return values, nil
}
