package coin

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/kit/kv"
)

type Config struct {
	// DEPRECATED: Still unsure what is it doing here
	MerchantID     string          `fig:"merchant_id,required"`
	Address        string          `fig:"address,required"`
	PrivateKey     string          `fig:"private_key,required"`
	PublicKey      string          `fig:"public_key,required"`
	// DEPRECATED: Still unsure what is it doing here
	IPNSecret      string          `fig:"ipn_secret,required"`
	RequestTimeout int64           `fig:"request_timeout,required"`

	//AddTxFee    bool `fig:"add_tx_fee,required"`
	//AutoConfirm bool `fig:"auto_confirm,required"`
}

func NewConfig(getter kv.Getter) (Config, error) {
	config := Config{}

	err := figure.
		Out(&config).
		From(kv.MustGetStringMap(getter, "coinpayments")).
		With(figure.BaseHooks, figure.BaseHooks).
		Please()
	if err != nil {
		return Config{}, errors.Wrap(err, "failed to figure out")
	}

	return config, nil
}
