package coin

import (
	"net/url"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/regources"
)

//Withdrawal is a struct containing arguments neede for Coinpayments'
// withdrawal request creation
type Withdrawal struct {
	Amount      string  `json:"amount"`
	Currency    string  `json:"currency"`
	Address     string  `json:"address"`
	AddTxFee    *string `json:"add_tx_fee,omitempty"`
	AutoConfirm *string `json:"auto_confirm,omitempty"`
}

//Validate checks val
func (w Withdrawal) Validate() error {
	return validation.ValidateStruct(&w,
		validation.Field(&w.Address, validation.Required),
		validation.Field(&w.Currency, validation.Required),
		validation.Field(&w.Amount, validation.Required, is.Float),
		validation.Field(&w.AddTxFee, validation.NilOrNotEmpty),
		validation.Field(&w.AutoConfirm, validation.NilOrNotEmpty),
	)
}

func (w Withdrawal) composeRequest() (url.Values, error) {
	err := w.Validate()
	if err != nil {
		return nil, errors.Wrap(err, "invalid withdrawal request parameters")
	}
	vals, err := convertToMap(w)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to compose withdrawal request")
	}
	vals["cmd"] = createWithdrawal
	return composeURLValues(vals), nil
}

//WithdrawalDetails contains data returned from Coinpayments
// on successful withdraw creation
type WithdrawalDetails struct {
	ID     string           `json:"id"`
	Status int              `json:"status"`
	Amount regources.Amount `json:"amount"`
}

type withdrawalResponse struct {
	Result WithdrawalDetails `json:"result"`
}

//WithdrawalInfo contains full information about Coinpayments'
//withdrawal request
type WithdrawalInfo struct {
	TimeCreated int32            `json:"time_created"`
	Status      int32            `json:"status"`
	Coin        string           `json:"coin"`
	Amount      regources.Amount `json:"amountf"`
	Address     string           `json:"payment_address"`
	SendAddress string           `json:"send_address"`
	SendTxID    string           `json:"send_txid"`
	StatusText  string           `json:"status_text"`
}

type withdrawalInfoResponse struct {
	Result WithdrawalInfo `json:"result"`
}
