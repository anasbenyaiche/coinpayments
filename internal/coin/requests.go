package coin

import (
	"net/url"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

//CreateWithdrawal sends request to Coinpayments to create new withdrawal with provided arguments
func (c *Connector) CreateWithdrawal(withdraw Withdrawal) (*WithdrawalDetails, error) {
	data, err := withdraw.composeRequest()
	if err != nil {
		return nil, errors.Wrap(err, "failed to compose withdrawal request", logan.F{
			"parameters": withdraw,
		})
	}
	fields := logan.F{
		"url_values": data,
	}

	result := withdrawalResponse{}
	err = c.performRequest(data, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create withdrawal", fields)
	}

	return &result.Result, nil
}

//GetWithdrawalInfo sends request to Coinpayments to get info on withdrawal by ID
func (c *Connector) GetWithdrawalInfo(wid string) (*WithdrawalInfo, error) {
	data := url.Values{}
	data.Set("cmd", getWithdrawalInfo)
	data.Set("id", wid)

	fields := logan.F{
		"url_values": data,
	}

	result := withdrawalInfoResponse{}
	err := c.performRequest(data, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get withdrawal info", fields)
	}

	return &result.Result, nil
}

//CreateTx sends request to Coinpayments to generate new transaction with provided args
func (c *Connector) CreateTx(transaction Transaction) (*TxDetails, error) {
	data, err := transaction.composeRequest()
	if err != nil {
		return nil, errors.Wrap(err, "failed to compose CreateTransaction request")
	}
	fields := logan.F{
		"url_values": data,
	}

	result := txResponse{}
	err = c.performRequest(data, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create transaction", fields)
	}

	return &result.Result, nil
}

//GetTxInfo returns info on single Coinpayments' transaction
func (c *Connector) GetTxInfo(txid string) (*TxInfo, error) {
	data := url.Values{}
	data.Set("cmd", getTxInfo)
	data.Add("txid", txid)
	data.Set("full", "1")

	fields := logan.F{
		"url_values": data,
	}

	result := txInfoResponse{}
	err := c.performRequest(data, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tx info", fields)
	}

	return &result.Result, nil
}
