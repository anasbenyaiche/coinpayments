package coin

import (
	"bytes"
	"net/http"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Note: real responses from coinpayments can be different from what specified in docs
var (
	createTxResp       = `{"error":"ok","result":{"amount":"%s","address":"%s","txn_id":"XXX","confirms_needed":"10","timeout":9000,"status_url":"https:\/\/www.coinpayments.net\/index.php?cmd=status&id=XXX&key=%s","qrcode_url":"https:\/\/www.coinpayments.net\/qrgen.php?id=XXX&key=%s"}}`
	createWithdrawResp = `{"error":"ok","result":{"id":"hex string","status":0,"amount":"%s"}}`
	withdrawInfoResp   = `{"error":"ok","result":{"time_created":1391924372,"status":2,"status_text":"Complete","coin":"BTC","amount":40000000,"amountf":"0.40000000","send_address":"1BitcoinAddress","send_txid":"hex_txid"}}`
	errorResp          = `{"error":"Error!!","result":{"time_created":1391924372,"status":2,"status_text":"Complete","coin":"BTC","amount":40000000,"amountf":"0.40000000","send_address":"1BitcoinAddress","send_txid":"hex_txid"}}`
	address            = "bc1SomeAddress"
	itemName           = "deposit"
	itemNumber         = "1"
)

func TestConnector_PrepareRequest(t *testing.T) {
	submitAddress := "http://localhost/submit"
	cc := Connector{
		config: Config{Address: submitAddress, PrivateKey: "private_key", PublicKey: "public_key", IPNSecret: "ipn_secret", RequestTimeout: 1000},
		client: http.DefaultClient,
	}
	t.Run("create tx", func(t *testing.T) {
		tx := Transaction{
			Address:           &address,
			ReceivingCurrency: "BTC",
			SendingCurrency:   "BTC",
			Amount:            "1.00003212",
			BuyerEmail:        "vasil@vmail.com",
			ItemName:          &itemName,
			ItemNumber:        &itemNumber,
		}
		expected := "address=bc1SomeAddress&amount=1.00003212&buyer_email=vasil%40vmail.com&cmd=create_transaction&currency1=BTC&currency2=BTC&custom=&item_name=deposit&item_number=1&key=public_key&version=1"
		data, err := tx.composeRequest()
		assert.NoError(t, err)
		urlRaw, r, err := cc.prepareRequest(data)
		assert.NoError(t, err)

		body, err := r.GetBody()
		assert.NoError(t, err)
		buf := new(bytes.Buffer)
		n, err := buf.ReadFrom(body)
		assert.NoError(t, err)
		assert.EqualValues(t, len([]byte(urlRaw)), n)

		assert.Equal(t, expected, urlRaw)
		assert.Equal(t, submitAddress, r.URL.String())
		assert.Equal(t, expected, buf.String())
	})

	t.Run("create withdrawal", func(t *testing.T) {
		autoConfirm := "1"
		addTxFee := "1"

		tx := Withdrawal{
			Address:     "bc1SomeAddress",
			Currency:    "BTC",
			Amount:      "1.00003212",
			AutoConfirm: &autoConfirm,
			AddTxFee:    &addTxFee,
		}

		expected := "add_tx_fee=1&address=bc1SomeAddress&amount=1.00003212&auto_confirm=1&cmd=create_withdrawal&currency=BTC&key=public_key&version=1"
		data, err := tx.composeRequest()
		assert.NoError(t, err)
		urlRaw, r, err := cc.prepareRequest(data)
		assert.NoError(t, err)

		body, err := r.GetBody()
		assert.NoError(t, err)
		buf := new(bytes.Buffer)
		n, err := buf.ReadFrom(body)
		assert.NoError(t, err)
		assert.EqualValues(t, len([]byte(urlRaw)), n)

		assert.Equal(t, expected, urlRaw)
		assert.Equal(t, submitAddress, r.URL.String())
		assert.Equal(t, expected, buf.String())
	})

	t.Run("get tx info", func(t *testing.T) {
		data := url.Values{}
		data.Set("cmd", getTxInfoMulti)
		data.Add("txid", "transaction_id")

		expected := "cmd=get_tx_info_multi&key=public_key&txid=transaction_id&version=1"
		urlRaw, r, err := cc.prepareRequest(data)
		assert.NoError(t, err)

		body, err := r.GetBody()
		assert.NoError(t, err)
		buf := new(bytes.Buffer)
		n, err := buf.ReadFrom(body)
		assert.NoError(t, err)
		assert.EqualValues(t, len([]byte(urlRaw)), n)

		assert.Equal(t, expected, urlRaw)
		assert.Equal(t, submitAddress, r.URL.String())
		assert.Equal(t, expected, buf.String())
	})

	t.Run("get withdrawal info", func(t *testing.T) {
		wid := "withdrawal_id"
		data := url.Values{}
		data.Set("cmd", getWithdrawalInfo)
		data.Set("id", wid)

		expected := "cmd=get_withdrawal_info&id=withdrawal_id&key=public_key&version=1"
		urlRaw, r, err := cc.prepareRequest(data)
		assert.NoError(t, err)

		body, err := r.GetBody()
		assert.NoError(t, err)
		buf := new(bytes.Buffer)
		n, err := buf.ReadFrom(body)
		assert.NoError(t, err)
		assert.EqualValues(t, len([]byte(urlRaw)), n)

		assert.Equal(t, expected, urlRaw)
		assert.Equal(t, submitAddress, r.URL.String())
		assert.Equal(t, expected, buf.String())
	})
}
