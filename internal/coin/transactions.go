package coin

import (
	"net/url"

	"github.com/go-ozzo/ozzo-validation"

	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

//Transaction contains arguments needed for Coinpayments'
//transaction creation
type Transaction struct {
	Amount            string  `json:"amount"`
	ReceivingCurrency string  `json:"currency1"`
	SendingCurrency   string  `json:"currency2"`
	BuyerEmail        string  `json:"buyer_email"`
	Address           *string `json:"address"`
	ItemName          *string `json:"item_name"`
	ItemNumber        *string `json:"item_number"`
	Custom            string  `json:"custom"`
}

func (t Transaction) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"amount":             t.Amount,
		"receiving_currency": t.ReceivingCurrency,
		"sending_currency":   t.SendingCurrency,
		"buyer_email":        t.BuyerEmail,
		"address":            t.Address,
		"item_name":          t.ItemName,
		"item_number":        t.ItemNumber,
		"custom":             t.Custom,
	}
}

func (t Transaction) composeRequest() (url.Values, error) {
	err := t.Validate()
	if err != nil {
		return nil, errors.Wrap(err, "invalid create transaction request params")
	}
	vals, err := convertToMap(t)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to compose withdrawal request")
	}

	vals["cmd"] = createTransaction
	return composeURLValues(vals), nil
}

// Validate checks necessary fields of struct
func (t Transaction) Validate() error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.Address, validation.NilOrNotEmpty),
		validation.Field(&t.ReceivingCurrency, validation.Required),
		validation.Field(&t.SendingCurrency, validation.Required),
		validation.Field(&t.Amount, validation.Required, is.Float),
		validation.Field(&t.BuyerEmail, validation.Required, is.Email),
		validation.Field(&t.ItemName, validation.NilOrNotEmpty),
		validation.Field(&t.ItemNumber, validation.NilOrNotEmpty),
	)
}

type txResponse struct {
	Result TxDetails `json:"result"`
}

//TxDetails contains info returned from Coinpayments on successful transaction creation
type TxDetails struct {
	Amount         string `json:"amount"`
	TxnID          string `json:"txn_id"`
	Address        string `json:"address"`
	ConfirmsNeeded string `json:"confirms_needed"`
	Timeout        int32  `json:"timeout"`
}

func (t TxDetails) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"amount":          t.Amount,
		"txn_id":          t.TxnID,
		"address":         t.Address,
		"confirms_needed": t.ConfirmsNeeded,
		"timeout":         t.Timeout,
	}
}

type txInfoResponse struct {
	Result TxInfo `json:"result"`
}

//TxInfo contains full information about Coinpayments' transaction
type TxInfo struct {
	ID           string  `json:"id"`
	TimeCreated  int32   `json:"time_created"`
	TimeExpires  int32   `json:"time_expires"`
	Status       int32   `json:"status"`
	Coin         string  `json:"coin"`
	Amount       string  `json:"amountf"`
	Received     string  `json:"receivedf"`
	RecvConfirms int32   `json:"recv_confirms"`
	Address      string  `json:"payment_address"`
	StatusText   string  `json:"status_text"`
	Invoice      *string `json:"invoice,omitempty"`
	Custom       *string `json:"custom,omitempty"`
	ItemName     *string `json:"item_name,omitempty"`
	ItemNumber   *string `json:"item_number,omitempty"`
}

func (t TxInfo) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"id":            t.ID,
		"time_created":  t.TimeCreated,
		"time_expires":  t.TimeExpires,
		"status":        t.Status,
		"coin":          t.Coin,
		"amount":        t.Amount,
		"received":      t.Received,
		"recv_confirms": t.RecvConfirms,
		"address":       t.Address,
		"status_text":   t.StatusText,
		"invoice":       t.Invoice,
		"custom":        t.Custom,
		"item_name":     t.ItemName,
		"item_number":   t.ItemNumber,
	}
}
