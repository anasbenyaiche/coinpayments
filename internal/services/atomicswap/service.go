package atomicswap

import (
	"context"
	"net"
	"net/http"

	"gitlab.com/distributed_lab/kit/copus/types"

	"gitlab.com/tokend/coinpayments/internal/forbill"

	"gitlab.com/tokend/coinpayments/internal/data"

	"gitlab.com/tokend/coinpayments/internal/coin"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/keypair"
)

type Service struct {
	log                       *logan.Entry
	listener                  net.Listener
	cop                       types.Copus
	signer                    keypair.Full
	coinpayments              *coin.Connector
	builder                   *xdrbuild.Builder
	horizon                   *horizon.Connector
	atomicSwapRequestProvider data.AtomicSwapRequestProvider
	identityProvider          data.IdentityProvider
	priceAsset                string
	forbillConnector          *forbill.Connector
}

func New(
	log *logan.Entry,
	listener net.Listener,
	cop types.Copus,
	signer keypair.Full,
	builder *xdrbuild.Builder,
	horizonConnector *horizon.Connector,
	coinpayments *coin.Connector,
	atomicSwapRequestProvider data.AtomicSwapRequestProvider,
	identityProvider data.IdentityProvider,
	priceAsset string,
	forbillConnetor *forbill.Connector,
) *Service {
	return &Service{
		log:                       log.WithField("service", "coinpayments_atomic_swap"),
		listener:                  listener,
		cop:                       cop,
		signer:                    signer,
		builder:                   builder,
		coinpayments:              coinpayments,
		horizon:                   horizonConnector,
		atomicSwapRequestProvider: atomicSwapRequestProvider,
		identityProvider:          identityProvider,
		priceAsset:                priceAsset,
		forbillConnector:          forbillConnetor,
	}
}

// Run is used to run coindeposit service which will listen for
// new deposit requests.
func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	r := s.prepareRouter()

	if err := http.Serve(s.listener, r); err != nil {
		panic(err)
	}
}
