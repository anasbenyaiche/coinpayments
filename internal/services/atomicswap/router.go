package atomicswap

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/distributed_lab/running"

	"gitlab.com/tokend/go/xdrbuild"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/tokend/coinpayments/internal/data"

	amount2 "gitlab.com/tokend/go/amount"

	"github.com/spf13/cast"
	regources "gitlab.com/tokend/regources/generated"

	"gitlab.com/tokend/go/xdr"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	validation "github.com/go-ozzo/ozzo-validation"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
)

func (s *Service) prepareRouter() chi.Router {
	router := chi.NewRouter()

	router.Post("/integrations/marketplace/buy", s.processPayment)

	if err := s.cop.RegisterChi(router); err != nil {
		panic(errors.Wrap(err, "failed to register in cop"))
	}

	return router
}

func (s *Service) processPayment(w http.ResponseWriter, r *http.Request) {
	request, err := NewPayRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	var txEnvelope xdr.TransactionEnvelope
	if err = xdr.SafeUnmarshalBase64(request.Tx, &txEnvelope); err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.New("tx should be a valid transaction"))...)
		return
	}
	if len(txEnvelope.Tx.Operations) == 0 {
		ape.RenderErr(w, problems.BadRequest(errors.New("tx don't have any operations"))...)
		return
	}
	if txEnvelope.Tx.Operations[0].Body.CreateAtomicSwapBidRequestOp == nil {
		ape.RenderErr(w, problems.BadRequest(errors.New("operation should be valid atomic swap operation"))...)
		return
	}
	atomicSwap := *txEnvelope.Tx.Operations[0].Body.CreateAtomicSwapBidRequestOp

	if atomicSwap.Request.QuoteAsset == xdr.AssetCode(s.priceAsset) {
		successResponse, errorResponse, err := s.forbillConnector.Pay(request.Tx, r.Header.Get("x-forwarded-for"))
		if err != nil {
			s.log.WithError(err).Error("failed to submit payment to forbill")
			ape.RenderErr(w, problems.InternalError())
			return
		}
		if errorResponse != nil {
			ape.RenderErr(w, errorResponse.Errors...)
			return
		}
		ape.Render(w, data.BuyResponse{
			Data: data.BuyResponseData{
				ID:   "1",
				Type: "buy-response",
				Attributes: data.BuyResponseAttributes{
					Type: data.BuyResponseTypeRedirect,
					Data: successResponse.Data.Attributes,
				},
			},
		})
		return
	}

	invoice, err := s.handlePayment(atomicSwap, txEnvelope.Tx.SourceAccount.Address())
	if err != nil {
		s.log.WithError(err).Error("failed to handle payment")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	result := s.horizon.Submitter().Submit(r.Context(), request.Tx)
	if result.Err != nil {
		s.log.WithError(result.Err).Error("transaction is invalid in some way")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	var txResult xdr.TransactionResult
	if err = xdr.SafeUnmarshalBase64(result.ResultXDR, &txResult); err != nil {
		s.log.WithError(err).Error("failed to unmarshal tx result")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	requestId := (*txResult.Result.Results)[0].Tr.CreateAtomicSwapBidRequestResult.Success.RequestId
	if err = s.approveAtomicSwapRequest(uint64(requestId), *invoice, r.Context()); err != nil {
		s.log.WithError(err).Error("failed to unmarshal response")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, data.BuyResponse{
		Data: data.BuyResponseData{
			ID:   strconv.FormatUint(uint64(requestId), 10),
			Type: "buy-response",
			Attributes: data.BuyResponseAttributes{
				Type: data.BuyResponseTypeCrypto,
				Data: invoice,
			},
		},
	})
}

type PayRequest struct {
	Tx string `json:"tx"`
}

func NewPayRequest(r *http.Request) (*PayRequest, error) {
	var request PayRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "failed to decode body")
	}

	return &request, request.Validate()
}

func (r *PayRequest) Validate() error {
	data := r
	return validation.ValidateStruct(
		data,
		validation.Field(&data.Tx, validation.Required),
	)
}

func (s *Service) handlePayment(atomicSwap xdr.CreateAtomicSwapBidRequestOp, source string) (*data.Invoice, error) {
	ask, err := s.atomicSwapRequestProvider.GetAtomicSwapAskRequest(strconv.FormatUint(uint64(atomicSwap.Request.AskId), 10))
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to get atomic swap ask %d", atomicSwap.Request.AskId))
	}
	if ask == nil {
		return nil, errors.New(fmt.Sprintf("atomic swap ask %d not found", atomicSwap.Request.AskId))
	}

	address, err := s.addressFromAsk(ask, string(atomicSwap.Request.QuoteAsset))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get addresses from ask")
	}

	amount, err := s.getPrice(ask, s.priceAsset, regources.Amount(atomicSwap.Request.BaseAmount))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get price")
	}

	identity, err := s.identityProvider.GetIdentity(source)
	if err != nil || identity == nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to find identity for source accout %s", source))
	}

	transaction := coin.Transaction{
		Amount:            amount.String(),
		ReceivingCurrency: s.priceAsset,
		SendingCurrency:   string(atomicSwap.Request.QuoteAsset),
		BuyerEmail:        identity.Attributes.Email,
		Address:           address,
	}

	result, err := s.coinpayments.CreateTx(transaction)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create coinpayments transaction")
	}

	roundedAmount, err := roundCoinpaymentsAmount(result.Amount)
	if err != nil {
		return nil, errors.Wrap(err, "unable to round coinpayments invoice amount")
	}

	return &data.Invoice{
		Address:        result.Address,
		Asset:          string(atomicSwap.Request.QuoteAsset),
		Amount:         roundedAmount,
		TxID:           result.TxnID,
		ConfirmsNeeded: result.ConfirmsNeeded,
		Timeout:        result.Timeout,
	}, nil
}

func roundCoinpaymentsAmount(amount string) (string, error) {
	parsedAmount, err := strconv.ParseFloat(amount, 32)
	if err != nil {
		return "", errors.Wrap(err, "unable to parse amount")
	}

	rounded := math.Ceil(parsedAmount*1000000) / 1000000
	return fmt.Sprintf("%f", rounded), nil
}

func (s *Service) getPrice(ask *regources.AtomicSwapAskResponse, assetCode string, baseAmount regources.Amount) (regources.Amount, error) {
	// Price will always be in price asset
	key := ask.Data.Relationships.QuoteAssets.Data[0]

	quoteAsset := ask.Included.MustQuoteAsset(key.GetKey())
	if quoteAsset == nil {
		return 0, errors.New("quote asset not found")
	}

	amount := (quoteAsset.Attributes.Price * baseAmount) / amount2.One

	//FIXME make it better
	return amount, nil
}

func (s *Service) addressFromAsk(ask *regources.AtomicSwapAskResponse, asset string) (*string, error) {
	var askDetails map[string]interface{}
	err := json.Unmarshal(ask.Data.Attributes.Details, &askDetails)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal ask details")
	}

	rawAddresses, ok := askDetails["destination"]
	if !ok {
		return nil, errors.New("no addresses in ask details")
	}

	addresses, err := cast.ToStringMapStringE(rawAddresses)
	if err != nil {
		return nil, errors.Wrap(err, "failed to cast addresses")
	}

	var address *string
	for currency, addr := range addresses {
		if currency == asset {
			address = &addr
			break
		}
	}

	if address == nil {
		return nil, errors.New("address not found")
	}

	return address, nil
}

func (s *Service) approveAtomicSwapRequest(id uint64, details data.Invoice, ctx context.Context) error {
	var atomicSwapBid *regources.ReviewableRequestResponse
	var err error
	running.WithThreshold(context.TODO(), s.log, "get-atomic-swap-bid", func(ctx context.Context) (bool, error) {
		atomicSwapBid, err = s.atomicSwapRequestProvider.GetAtomicSwapBidRequest(strconv.FormatUint(id, 10))
		if err != nil {
			err = errors.Wrap(err, "failed to get atomic swap bid request")
			return false, err
		}
		if atomicSwapBid == nil {
			err = errors.New("atomic swap bid not found")
			return false, err
		}

		return true, nil
	}, 1*time.Second, 5*time.Second, 5)
	if err != nil {
		return err
	}

	detailsData, err := json.Marshal(&details)

	envelope, err := s.builder.Transaction(s.signer).Op(xdrbuild.ReviewRequest{
		ID:      id,
		Hash:    &atomicSwapBid.Data.Attributes.Hash,
		Details: xdrbuild.AtomicSwapDetails{},
		Action:  xdr.ReviewRequestOpActionApprove,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      data.TaskPendingForTx,
			TasksToRemove:   atomicSwapBid.Data.Attributes.PendingTasks,
			ExternalDetails: string(detailsData),
		},
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to create envelope")
	}

	submitResult := s.horizon.Submitter().Submit(ctx, envelope)
	if submitResult.Err != nil {
		return errors.Wrap(submitResult.Err, "failed to submit envelope")
	}
	return nil
}
