package coinwithdraw

import (
	"context"
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
)

const (
	horizonEndpoint = "http://localhost:8000/"
	seed            = "SAF6ISUCLIXQTUR3CQRRDXWROJ4OMX2FDPUQE6XPYBXBEI6RKQLHG3QC"
	balanceId       = "BDIZTXKT6C3SX7LLIZBEKYYT7YGCXYQW3YW7A4QVBVHV53MSE7JBW7MK"

	permanentlyRejectedState = "permanently_rejected"
)

func TestWithdrawDetails(t *testing.T) {
	cases := []*struct {
		Name                string
		CreatorDetails      string
		ReviewableRequestId uint64
		ExpectedState       string
	}{
		{
			Name:           "Invalid json format",
			CreatorDetails: "{\"some_key\":\"some_string\"}",
			ExpectedState:  permanentlyRejectedState,
		},
		{
			Name:           "Invalid address",
			CreatorDetails: "{\"address\":\"some_address\"}",
			ExpectedState:  permanentlyRejectedState,
		},
	}

	endpoint, _ := url.Parse(horizonEndpoint)
	signer := keypair.MustParseSeed(seed)
	hrz := horizon.NewConnector(endpoint).WithSigner(signer)
	builder, err := hrz.TXBuilder()
	if err != nil {
		t.Fatal(err)
	}

	for _, c := range cases {
		id, err := submitWithdrawalRequest(10000000, c.CreatorDetails, builder, hrz, signer)
		require.NoError(t, err, "failed to create withdrawal request")
		c.ReviewableRequestId = *id
	}

	time.Sleep(30 * time.Second)

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			reviewableRequest, err := hrz.Operations().GetRequestByID(c.ReviewableRequestId)
			require.NoError(t, err, "failed to get reviewable request")
			require.Equal(t, c.ExpectedState, reviewableRequest.StateName)
		})
	}
}

// Helpers

func submitWithdrawalRequest(amount uint64, creatorDetails string,
	txBuilder *xdrbuild.Builder, hrz *horizon.Connector, signer keypair.Full) (*uint64, error) {

	env, err := txBuilder.Transaction(signer).Op(withdrawalRequest{
		BalanceId:      balanceId,
		Amount:         amount,
		CreatorDetails: creatorDetails,
	}).Sign(signer).Marshal()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal create account tx")
	}

	result := hrz.Submitter().Submit(context.Background(), env)
	if err != nil {
		return nil, errors.Wrap(result.Err,
			fmt.Sprintf("failed to submit tx: %s, %s, %s", result.TXCode, result.OpCodes, result.Err))
	}

	var txResult xdr.TransactionResult
	err = xdr.SafeUnmarshalBase64(result.ResultXDR, &txResult)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal transaction response")
	}

	requestId := uint64((*txResult.Result.Results)[0].Tr.CreateWithdrawalRequestResult.Success.RequestId)

	return &requestId, nil
}

type withdrawalRequest struct {
	BalanceId      string
	Amount         uint64
	CreatorDetails string
}

func (op withdrawalRequest) XDR() (*xdr.Operation, error) {
	var balanceId xdr.BalanceId
	if err := balanceId.SetString(op.BalanceId); err != nil {
		return nil, errors.Wrap(err, "failed to set balance id")
	}

	withdrawalRequest := xdr.WithdrawalRequest{
		Balance:         balanceId,
		Amount:          xdr.Uint64(op.Amount),
		CreatorDetails:  xdr.Longstring(op.CreatorDetails),
		UniversalAmount: xdr.Uint64(0),
	}

	return &xdr.Operation{
		Body: xdr.OperationBody{
			Type: xdr.OperationTypeCreateWithdrawalRequest,
			CreateWithdrawalRequestOp: &xdr.CreateWithdrawalRequestOp{
				Request: withdrawalRequest,
			},
		},
	}, nil
}
