package coinwithdraw

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/regources"
)

const (
	requestStatePending int32 = 1
)

//Errors returned by Service
var (
	ErrMissingRequestInDetails = errors.New("missing WithdrawRequest in the Request")

	ErrMissingAddress    = errors.New("missing address field in the ExternalDetails json")
	ErrAddressNotAString = errors.New("address field in ExternalDetails is not a string")
)

// getWithdrawalAddress obtains withdrawal Address from the `address` field of the ExternalDetails
// of Withdraw in Request Details.
//
// getWithdrawalAddress does work well with both Withdraw and TwoStepWithdraw Requests.
//
// Returns error if no `address` field in the ExternalDetails map or if the field is not a string.
// Only returns errors with causes:
// - ErrMissingRequestInDetails
// - ErrMissingAddress
// - ErrAddressNotAString.
func getWithdrawalAddress(request regources.ReviewableRequest) (string, error) {
	if request.Details.Withdraw == nil {
		return "", ErrMissingRequestInDetails
	}

	addrValue, ok := request.Details.Withdraw.ExternalDetails["address"]
	if !ok {
		return "", ErrMissingAddress
	}

	addr, ok := addrValue.(string)
	if !ok {
		return "", errors.From(ErrAddressNotAString, logan.F{"raw_address_value": addrValue})
	}

	return addr, nil

}

func getWithdrawAmount(request regources.ReviewableRequest) (regources.Amount, error) {
	if request.Details.Withdraw != nil {
		return request.Details.Withdraw.Amount, nil
	}

	return 0, ErrMissingRequestInDetails
}
