package coinwithdraw

import (
	"context"
	"time"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/regources"
	"gitlab.com/tokend/coinpayments/internal/horizonconnector"
)

const (
	taskTrySendToCP               uint32 = 2048  // 2^11
	taskApproveSuccessfulSendToCP uint32 = 4096  // 2^12
	taskApproveWithdrawCompleted  uint32 = 8192  // 2^13
	taskManualReview              uint32 = 16384 // 2^14
)

// RequestListener is the interface, which must be implemented
// by streamer of Horizon Requests, which parametrize Service.
type RequestListener interface {
	StreamWithdrawalRequests(ctx context.Context, opts horizon.WithdrawalRequestStreamingOpts) <-chan horizon.ReviewableRequestEvent
}

// RequestsConnector is the interface implemented in HorizonConnector,
// it allows to obtain request by its ID and is used for fetching Withdraw request
// after preliminary approve (TwoStep request).
type RequestsConnector interface {
	GetRequestByID(requestID uint64) (*regources.ReviewableRequest, error)
}

type TXSubmitter interface {
	Submit(ctx context.Context, envelope string) horizon.SubmitResult
}

type Coinpayments interface {
	CreateWithdrawal(withdraw coin.Withdrawal) (*coin.WithdrawalDetails, error)
	GetWithdrawalInfo(wid string) (*coin.WithdrawalInfo, error)
}

type AssetProvider interface {
	// ByCode returns nil,nil if the Asset with the AssetCode is not found.
	ByCode(code string) (*horizonconnector.Asset, error)
}

type BalanceProvider interface {
	// AssetByBalanceID returns nil,nil if such Balances was not found
	AssetByBalanceID(balanceID string) (*string, error)
}

// Service is withdraw service for use with Coinpayments,
// which approves or rejects WithdrawRequest
type Service struct {
	log         *logan.Entry
	source      keypair.Address
	signer      keypair.Full
	addTxFee    bool
	autoConfirm bool

	requestListener   RequestListener
	requestsConnector RequestsConnector
	txSubmitter       TXSubmitter
	xdrbuilder        *xdrbuild.Builder
	coinpayments      Coinpayments
	assets            AssetProvider
	balances          BalanceProvider
}

// New is constructor for Service.
func New(
	log *logan.Entry,
	source keypair.Address,
	signer keypair.Full,
	addTxFee bool,
	autoConfirm bool,
	requestListener RequestListener,
	requestsConnector RequestsConnector,
	txSubmitter TXSubmitter,
	builder *xdrbuild.Builder,
	coinpayments Coinpayments,
	assets AssetProvider,
	balances          BalanceProvider,
) *Service {
	return &Service{
		log:               log.WithField("service", "coinwithdraw"),
		source:            source,
		signer:            signer,
		addTxFee:          addTxFee,
		autoConfirm:       autoConfirm,
		requestListener:   requestListener,
		requestsConnector: requestsConnector,
		txSubmitter:       txSubmitter,
		xdrbuilder:        builder,
		coinpayments:      coinpayments,
		assets:            assets,
		balances:          balances,
	}
}

// Run is a blocking method, it returns only when ctx closes.
func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	running.WithBackOff(ctx, s.log, "coinpayments_withdraw", s.processAllWithdrawalsOnce, 5*time.Second, 5*time.Second, 10*time.Minute)
}

func (s *Service) processAllWithdrawalsOnce(ctx context.Context) error {
	requestEvents := s.requestListener.StreamWithdrawalRequests(ctx, horizon.WithdrawalRequestStreamingOpts{
		StopOnEmptyPage: true,
		ReverseOrder:    false,
		RequestState:    requestStatePending,
	})

	for {
		if running.IsCancelled(ctx) {
			return nil
		}

		select {
		case <-ctx.Done():
			s.log.Info("Ctx is cancelled, will not process any more Requests, stopping now.")
			return nil
		case event, ok := <-requestEvents:
			if !ok {
				time.Sleep(5 * time.Second)
				return nil
			}

			request, err := event.Unwrap()
			if err != nil {
				return errors.Wrap(err, "received error from IssuanceRequest stream")
			}
			if request == nil {
				return errors.New("received empty request")
			}

			if err := s.processRequest(ctx, *request); err != nil {
				s.log.WithError(err).WithFields(logan.F{
					"request": request,
					"request_id": request.ID,
				}).Error("Failed to process request.")
				continue
			}
		}
	}
}

func (s *Service) processRequest(ctx context.Context, request regources.ReviewableRequest) error {
	if !s.isWithdrawPendingRequest(request) {
		return nil
	}

	log := s.log.WithFields(logan.F{
		"request": request,
	})

	log.WithField("pending_tasks", request.PendingTasks).Debug("Found a pending Withdraw Request.")

	ok, err := s.proveCoinpayments(request)
	if err != nil {
		return errors.Wrap(err, "failed to check whether Asset can be processed through Coinpayments")
	}
	if !ok {
		log.Debug("The pending Withdraw Request is not a Coinpayments processable.")
		return nil
	}

	if isSetMask(taskManualReview, request.PendingTasks) {
		log.Debug("Manual review is set for the WithdrawRequest - not looking into it.")
		return nil
	}

	err = s.processValidPendingRequest(ctx, request)
	if err != nil {
		return errors.Wrap(err, "failed to process valid pending request")
	}

	return nil
}

func (s *Service) isWithdrawPendingRequest(request regources.ReviewableRequest) bool {
	if request.State != requestStatePending {
		// State is not pending
		return false
	}

	if request.Details.RequestType != int32(xdr.ReviewableRequestTypeCreateWithdraw) {
		return false
	}

	return true
}

// ProveCoinpayments returns false,nil if asset was not found or is not a Coinpayments asset.
func (s *Service) proveCoinpayments(request regources.ReviewableRequest) (bool, error) {
	assetCode, err := s.getAssetCode(request.Details.Withdraw.BalanceID)
	if err != nil {
		return false, errors.Wrap(err, "failed to get Asset code by the BalanceID of the Withdraw request")
	}

	asset, err := s.assets.ByCode(assetCode)
	if err != nil {
		return false, errors.Wrap(err, "failed to get asset by code")
	}
	if asset == nil {
		return false, nil
	}

	return s.isCoinpayments(*asset), nil
}

// This is method, not a function because implementation could potentially change and a new one would require the Service to be accessible.
func (s *Service) isCoinpayments(asset horizonconnector.Asset) bool {
	return asset.Details.IsCoinpayments
}

