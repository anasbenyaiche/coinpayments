package coinwithdraw

import (
	"regexp"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

var rejectReasons = map[string]string{
	`That amount is larger than your balance!`:             `wallet has insufficient amount`,
	`That is not a valid address for that coin!`:           `invalid address withdraw address`,
	`Minimum withdrawal amount is (\d.*\d) for this coin!`: `withdraw amount too low`,
	`does not support withdrawals!`:                       `the asset is not withdrawable in Coinpayments`,
}

func getReason(err error) (string, bool) {
	cause := errors.Cause(err).Error()

	for k, v := range rejectReasons {
		if ok, _ := regexp.MatchString(k, cause); ok {
			return v, true
		}
	}
	return "", false
}
