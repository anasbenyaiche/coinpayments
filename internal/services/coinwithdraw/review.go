package coinwithdraw

import (
	"context"
	"time"

	"gitlab.com/distributed_lab/running"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/regources"
)

//Status codes for coinpayments withdrawal requests
const (
	CoinPaymentsWithdrawCompleted              = 2
	CoinPaymentsWithdrawPending                = 0
	CoinPaymentsWithdrawWaitingForEmailConfirm = 1
	CoinPaymentsWithdrawCancelled              = -1
)

func (s *Service) processValidPendingRequest(ctx context.Context, request regources.ReviewableRequest) error {
	log := s.log.WithField("request", request)

	withdrawAddress, err := getWithdrawalAddress(request)
	if err != nil {
		log.Info("Withdrawal has invalid details")
		s.permanentlyRejectWithdrawRequest(ctx, request, "invalid withdraw request details format")
		return nil
	}

	amount, err := getWithdrawAmount(request)
	if err != nil {
		return errors.Wrap(err, "failed to get Withdraw Amount")
	}

	switch {
	case isSetMask(taskTrySendToCP, request.PendingTasks):
		log.Debug("This Withdrawal is not yet tried to submit to Coinpayments, will now try to create Coinpayments Tx.")
		return s.createTransactionAndReviewReq(ctx, request, withdrawAddress, amount)
	case isSetMask(taskApproveWithdrawCompleted, request.PendingTasks) &&
		!isSetMask(taskApproveSuccessfulSendToCP, request.PendingTasks):

		log.Debug("This Withdrawal is already checked to exist in Coinpayments and waiting for its status to become final.")
		err := s.checkCPStatusReviewRequest(ctx, request)
		if err != nil {
			return errors.Wrap(err, "failed to check status of the Withdraw in Coinpayments or review the WithdrawRequest")
		}

		// Not making log here, because checkCPStatusReviewRequest could actually just do nothing, if Withdrawal is still pending.
		return nil
	case isSetMask(taskApproveSuccessfulSendToCP, request.PendingTasks):
		// taskApproveSuccessfulSendToCP but taskApproveWithdrawCompleted is not set - it means
		// that Coinpayments Tx has not been created successfully.
		err := s.setManualReview(ctx, request, request.Details.Withdraw.ExternalDetails)
		if err != nil {
			return errors.Wrap(err, "failed to set ManualReview")
		}

		log.Info("Successfully set ManualReview for the WithdrawRequest.")
		return nil
	default:
		log.WithField("pending_tasks", request.PendingTasks).
			Debug("Not processing this WithdrawRequest due to its PendingTasks.")
		return nil
	}
}

func (s *Service) createTransactionAndReviewReq(ctx context.Context, request regources.ReviewableRequest, withdrawAddress string, amount regources.Amount) error {
	err := s.reviewWithdraw(ctx, request, taskApproveSuccessfulSendToCP, taskTrySendToCP, request.Details.Withdraw.ExternalDetails)
	if err != nil {
		return errors.Wrap(err, "failed to approve WithdrawRequest")
	}

	// We are intentionally using asset-codes from TokenD when talking to Coinpayments - asset codes are assumed to be the same.
	assetCode, err := s.getAssetCode(request.Details.Withdraw.BalanceID)
	if err != nil {
		return errors.Wrap(err, "failed to get Asset code by the BalanceID of the Withdraw request")
	}

	extDetails := request.Details.Withdraw.ExternalDetails
	details, err := s.submitCoinpaymentsWithdraw(assetCode, withdrawAddress, amount)
	if err != nil {
		s.log.WithError(err).Warn("Failed to create withdrawal at Coinpayments.")

		if reason, ok := getReason(err); ok {
			s.log.WithFields(logan.F{
				"request":       request,
				"reject_reason": reason,
			}).WithError(err).Info("Trying to reject WithdrawRequest.")
			return s.permanentlyRejectWithdrawRequest(ctx, request, reason)
		}

		s.log.WithFields(logan.F{
			"request": request,
		}).WithError(err).Warn("Failed to form a RejectReason from the Coinpayments error response for submitWithdraw.")
		// We intentionally ignore error as we can't handle it anyhow if it occurs,
		// we are returning error anyway regardless of the result of this reviewing.
		_ = s.reviewWithdraw(ctx, request, taskManualReview, 0, request.Details.Withdraw.ExternalDetails)

		return errors.Wrap(err, "failed to send withdrawal request to Coinpayments")
	}
	// FIXME: CoinPayments charges variable fee
	// if details.Amount != amount {
	// 	_ = s.reviewWithdraw(ctx, request, taskManualReview, 0, request.Details.Withdraw.ExternalDetails)
	// 	return errors.From(errors.New("Amount mismatched"), logan.F{
	// 		"expected": amount,
	// 		"actual":   details.Amount,
	// 	})
	// }

	extDetails["wid"] = details.ID
	err = s.reviewWithdraw(ctx, request, taskApproveWithdrawCompleted, taskApproveSuccessfulSendToCP, extDetails)
	if err != nil {
		return errors.Wrap(err, "failed to review request (adding wid into ExternalDetails)")
	}

	s.ensureIngested(ctx, request.ID)

	return nil
}

func (s *Service) getAssetCode(balanceID string) (string, error) {
	asset, err := s.balances.AssetByBalanceID(balanceID)
	if err != nil {
		return "", errors.Wrap(err, "failed to get asset by BalanceID")
	}
	if asset == nil {
		return "", errors.New("Balance was not found")
	}

	return *asset, nil
}

// EnsureIngested is a blocking method which will only return if successfully checked that RequestID is in Blockchain.
func (s *Service) ensureIngested(ctx context.Context, requestID uint64) {
	running.UntilSuccess(ctx, s.log, "ingest_ensurer", func(ctx context.Context) (b bool, err error) {
		req, err := s.requestsConnector.GetRequestByID(requestID)
		if err != nil {
			return false, errors.Wrap(err, "failed to get request by ID", logan.F{
				"request_id": requestID,
			})
		}

		switch {
		case isSetMask(taskApproveWithdrawCompleted, req.PendingTasks):
			return true, nil
		case isSetMask(taskApproveSuccessfulSendToCP, req.PendingTasks):
			return false, nil
		default:
			s.log.WithFields(logan.F{
				"request_id":     requestID,
				"expected_tasks": taskApproveWithdrawCompleted,
				"actual_tasks":   req.PendingTasks,
			}).Warn("Unexpected request state")
			return true, nil
		}
	}, time.Second, 5*time.Second)
}

func (s *Service) submitCoinpaymentsWithdraw(asset, address string, withdrawAmount regources.Amount) (*coin.WithdrawalDetails, error) {
	req := coin.Withdrawal{
		Amount:   withdrawAmount.String(),
		Address:  address,
		Currency: asset,
	}
	yes := "1"

	if s.addTxFee {
		req.AddTxFee = &yes
	}
	if s.autoConfirm {
		req.AutoConfirm = &yes
	}

	res, err := s.coinpayments.CreateWithdrawal(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create withdraw req")
	}

	return res, nil
}

func (s *Service) checkCPStatusReviewRequest(ctx context.Context, request regources.ReviewableRequest) error {
	info, err := s.coinpayments.GetWithdrawalInfo(getWID(request.ExternalDetails))
	if err != nil {
		return errors.Wrap(err, "failed to get withdrawal info from Coinpayments")
	}

	log := s.log.WithFields(logan.F{
		"request": request,
	})

	switch info.Status {
	case CoinPaymentsWithdrawCompleted:
		err = s.reviewWithdraw(ctx, request, 0, taskApproveWithdrawCompleted, map[string]interface{}{
			"sent_txid": info.SendTxID,
		})
		if err != nil {
			return errors.Wrap(err, "failed to review WithdrawRequest", logan.F{
				"request":           request,
				"coinpayments_info": info,
			})
		}

		log.Info("Coinpayments has successfully completed the Withdrawal, WithdrawRequest is successfully reviewed")
		return nil
	case CoinPaymentsWithdrawPending:
		return nil
	case CoinPaymentsWithdrawWaitingForEmailConfirm:
		return nil
	case CoinPaymentsWithdrawCancelled:
		return s.setManualReview(ctx, request, map[string]interface{}{})
	default:
		log.WithField("cp_withdrawal_status", info.Status).Error("Unexpected withdraw status from Coinpayments.")
		return s.setManualReview(ctx, request, map[string]interface{}{})
	}

}

func (s *Service) setManualReview(ctx context.Context, request regources.ReviewableRequest, extDetails map[string]interface{}) error {
	err := s.reviewWithdraw(ctx, request, taskManualReview, 0, extDetails)
	if err != nil {
		return errors.Wrap(err, "failed to set manual review task")
	}
	return nil
}

func (s *Service) reviewWithdraw(ctx context.Context, request regources.ReviewableRequest, toAdd, toRemove uint32, extDetails map[string]interface{}) error {
	bb, err := json.Marshal(extDetails)
	if err != nil {
		return errors.Wrap(err, "failed to marshal external bb map")
	}

	envelope, err := s.xdrbuilder.Transaction(s.source).Op(xdrbuild.ReviewRequest{
		ID:     request.ID,
		Reason: "",
		Hash:   &request.Hash,
		Action: xdr.ReviewRequestOpActionApprove,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      toAdd,
			TasksToRemove:   toRemove,
			ExternalDetails: string(bb),
		},
		Details: xdrbuild.WithdrawalDetails{
			ExternalDetails: string(bb),
		},
	}).Sign(s.signer).Marshal()

	if err != nil {
		return errors.Wrap(err, "failed to review Withdraw request")
	}

	result := s.txSubmitter.Submit(ctx, envelope)
	if result.Err != nil {
		return errors.Wrap(result.Err, "failed to submit envelope", logan.F{
			"result_xdr": result.ResultXDR,
			"tx_code":    result.TXCode,
		})
	}

	return nil
}

func (s *Service) permanentlyRejectWithdrawRequest(
	ctx context.Context,
	request regources.ReviewableRequest,
	rejectReason string,
) error {
	signedEnvelope, err := s.xdrbuilder.Transaction(s.source).Op(xdrbuild.ReviewRequest{
		ID:     request.ID,
		Hash:   &request.Hash,
		Action: xdr.ReviewRequestOpActionPermanentReject,
		Details: xdrbuild.WithdrawalDetails{
			ExternalDetails: "{}",
		},
		Reason: rejectReason,
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "Failed to marshal signed Envelope")
	}

	result := s.txSubmitter.Submit(ctx, signedEnvelope)
	if result.Err != nil {
		return errors.Wrap(err, "Error submitting signed Envelope to Horizon")
	}

	return nil
}

func getWID(extDetails map[string]interface{}) string {
	ext := struct {
		Data []struct {
			WID *string `json:"wid"`
		} `json:"data"`
	}{}
	bb, err := json.Marshal(extDetails)
	if err != nil {
		return ""
	}

	err = json.Unmarshal(bb, &ext)
	if err != nil {
		return ""
	}

	for i := len(ext.Data) - 1; i >= 0; i-- {
		if ext.Data[i].WID != nil {
			return *ext.Data[i].WID
		}
	}

	return ""
}

func isSetMask(mask, allTasks uint32) bool {
	return mask&allTasks != 0
}
