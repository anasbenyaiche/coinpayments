package atomicswapchecker

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"
	"time"

	amount2 "gitlab.com/tokend/go/amount"

	"github.com/spf13/cast"

	"gitlab.com/tokend/horizon-connector"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/tokend/coinpayments/internal/data"

	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"

	"gitlab.com/tokend/go/xdr"

	"gitlab.com/distributed_lab/logan/v3/errors"

	regources "gitlab.com/tokend/regources/generated"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/running"
)

type Service struct {
	log                          *logan.Entry
	reviewableRequestStreamer    *data.ReviewableRequestStreamer
	signer                       keypair.Full
	atomicSwapAskRequestProvider data.AtomicSwapRequestProvider

	builder          *xdrbuild.Builder
	identityProvider data.IdentityProvider
	coinpayments     *coin.Connector
	horizon          *horizon.Connector
}

func New(
	log *logan.Entry,
	reviewableRequestStreamer *data.ReviewableRequestStreamer,
	signer keypair.Full,
	atomicSwapAskRequestProvider data.AtomicSwapRequestProvider,
	builder *xdrbuild.Builder,
	identityProvider data.IdentityProvider,
	coinpayments *coin.Connector,
	horizon *horizon.Connector,
) *Service {
	return &Service{
		log:                          log,
		reviewableRequestStreamer:    reviewableRequestStreamer,
		signer:                       signer,
		atomicSwapAskRequestProvider: atomicSwapAskRequestProvider,
		builder:                      builder,
		identityProvider:             identityProvider,
		coinpayments:                 coinpayments,
		horizon:                      horizon,
	}
}

const reviewableRequestStatePending = "pending"

func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	running.WithBackOff(ctx, s.log, "atomic-swap-checker", s.startChecker, 1*time.Second, 10*time.Second, 10*time.Second)
}

func (s *Service) startChecker(ctx context.Context) error {
	streamer := s.reviewableRequestStreamer.New().
		WithState(data.FilterStatePending).
		WithPendingTasksNotSet(data.TaskPendingForTx)

	requestsStream := streamer.Stream()
	errs := streamer.Errors()

	for {
		if running.IsCancelled(ctx) {
			return nil
		}

		select {
		case requests := <-requestsStream:
			for _, request := range requests.Data {
				details := requests.Included.MustCreateAtomicSwapBidRequest(request.Relationships.RequestDetails.Data.GetKey())
				if details == nil {
					s.log.WithFields(logan.F{
						"request_id": request.ID,
						"request":    request,
					}).Error("failed to get reviewable request details")
					continue
				}

				if err := s.processRequest(ctx, request, details); err != nil {
					s.log.WithError(err).WithFields(logan.F{
						"request_id": request.ID,
						"request":    request,
					}).Error("failed to process request")
					continue
				}
			}
		case err := <-errs:
			if err == data.ErrEmptyPage {
				return nil
			}
			return errors.Wrap(err, "reviewable request streamer send error")
		}
	}
}

func (s *Service) processRequest(ctx context.Context, request regources.ReviewableRequest, details *regources.CreateAtomicSwapBidRequest) error {
	if request.Attributes.State != reviewableRequestStatePending || request.Attributes.XdrType != xdr.ReviewableRequestTypeCreateAtomicSwapBid {
		return nil
	}

	// normally error should never happen
	requestID, err := strconv.Atoi(request.ID)
	if err != nil {
		return errors.Wrap(err, "failed to convert request id")
	}

	// get related ask
	ask, err := s.atomicSwapAskRequestProvider.GetAtomicSwapAskRequest(details.Relationships.Ask.Data.ID)
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to get ask request")
		return s.handleErrWithReject(ctx, data.ErrRelatedAskNotFound, requestID, &request)
	}

	if ask == nil {
		return s.handleErrWithReject(ctx, data.ErrRelatedAskNotFound, requestID, &request)
	}

	// details should contain addresses with assets
	// get one that matches the asset
	address, err := s.addressFromAsk(ask, strings.Split(details.Relationships.QuoteAsset.Data.ID, ":")[0])
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to get addresses from ask")
		return s.handleErrWithReject(ctx, data.ErrNoAddresses, requestID, &request)
	}

	// get user's email from identity
	identity, err := s.identityProvider.GetIdentity(request.Relationships.Requestor.Data.ID)
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to get identity")
		return s.handleErrWithReject(ctx, data.ErrIdentityNotFound, requestID, &request)
	}

	if identity == nil {
		return s.handleErrWithReject(ctx, data.ErrIdentityNotFound, requestID, &request)
	}

	// get ask request that contains information about asset price
	amount, err := s.getAmount(ask, details)
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to get amount")
		return s.handleErrWithReject(ctx, data.ErrInvalidAmount, requestID, &request)
	}

	transaction := coin.Transaction{
		Amount:            amount.String(),
		ReceivingCurrency: strings.Split(details.Relationships.QuoteAsset.Data.ID, ":")[0],
		SendingCurrency:   strings.Split(details.Relationships.QuoteAsset.Data.ID, ":")[0],
		BuyerEmail:        identity.Attributes.Email,
		ItemName:          &request.Attributes.Hash,
		Address:           address,
	}

	result, err := s.coinpayments.CreateTx(transaction)
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to create transaction")
		return s.handleErrWithReject(ctx, data.ErrCoinpaymentTransaction, requestID, &request)
	}

	detailsData, err := json.Marshal(&data.ExternalDetails{
		Invoice: data.Invoice{
			Address:        result.Address,
			Asset:          strings.Split(details.Relationships.QuoteAsset.Data.ID, ":")[0],
			Amount:         result.Amount,
			TxID:           result.TxnID,
			ConfirmsNeeded: result.ConfirmsNeeded,
			Timeout:        result.Timeout,
		},
	})
	if err != nil {
		s.log.WithError(err).WithField("request_id", requestID).Error("failed to marshal invoice")
		return s.handleErrWithReject(ctx, data.ErrFailedToCreateInvoice, requestID, &request)
	}

	// approve request with task pendingForTx
	envelope, err := s.builder.Transaction(s.signer).Op(xdrbuild.ReviewRequest{
		ID:      uint64(requestID),
		Hash:    &request.Attributes.Hash,
		Details: xdrbuild.AtomicSwapDetails{},
		Action:  xdr.ReviewRequestOpActionApprove,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      data.TaskPendingForTx,
			TasksToRemove:   request.Attributes.PendingTasks,
			ExternalDetails: string(detailsData),
		},
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to create envelope")
	}

	submitResult := s.horizon.Submitter().Submit(ctx, envelope)
	if submitResult.Err != nil {
		return errors.Wrap(submitResult.Err, "failed to submit envelope", logan.F{
			"submit_result": submitResult,
		})
	}
	return nil
}

func (s *Service) getAmount(ask *regources.AtomicSwapAskResponse, details *regources.CreateAtomicSwapBidRequest) (regources.Amount, error) {
	var key *regources.Key
	for _, asset := range ask.Data.Relationships.QuoteAssets.Data {
		if asset.ID == details.Relationships.QuoteAsset.Data.ID {
			key = &asset
		}
	}

	if key == nil {
		return 0, errors.New("quote asset doesn't match")
	}

	quoteAsset := ask.Included.MustQuoteAsset(key.GetKey())
	if quoteAsset == nil {
		return 0, errors.New("quote asset not found")
	}

	amount := (quoteAsset.Attributes.Price * details.Attributes.BaseAmount) / amount2.One

	//FIXME make it better
	return amount, nil
}

func (s *Service) addressFromAsk(ask *regources.AtomicSwapAskResponse, asset string) (*string, error) {
	var askDetails map[string]interface{}
	err := json.Unmarshal(ask.Data.Attributes.Details, &askDetails)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal ask details")
	}

	rawAddresses, ok := askDetails["addresses"]
	if !ok {
		return nil, errors.New("no addresses in ask details")
	}

	addresses, err := cast.ToStringMapStringE(rawAddresses)
	if err != nil {
		return nil, errors.Wrap(err, "failed to cast addresses")
	}

	var address *string
	for currency, addr := range addresses {
		if currency == asset {
			address = &addr
			break
		}
	}

	if address == nil {
		return nil, errors.New("address not found")
	}

	return address, nil
}

func (s *Service) handleErrWithReject(ctx context.Context, err error, requestID int, request *regources.ReviewableRequest) error {
	var rejectReason string
	switch err.(type) {
	case data.Error:
		rejectReason = err.Error()
	default:
		rejectReason = data.ErrInternalProblems.Error()
	}
	rejectErr := s.rejectRequest(ctx, requestID, request, rejectReason)
	if rejectErr != nil {
		return errors.Wrap(err, "failed to reject request")
	}
	return err
}

func (s *Service) rejectRequest(ctx context.Context, requestID int, request *regources.ReviewableRequest, rejectReason string) error {
	envelope, err := s.builder.Transaction(s.signer).Op(xdrbuild.ReviewRequest{
		ID:      uint64(requestID),
		Hash:    &request.Attributes.Hash,
		Details: xdrbuild.AtomicSwapDetails{},
		Action:  xdr.ReviewRequestOpActionPermanentReject,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:    data.TaskPendingForTx,
			TasksToRemove: 0,
		},
		Reason: rejectReason,
	}).Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to marshal tx")
	}

	submitResult := s.horizon.Submitter().Submit(ctx, envelope)
	if submitResult.Err != nil {
		return errors.Wrap(submitResult.Err, "failed to submit envelope", logan.F{
			"submit_result": submitResult,
		})
	}
	return nil
}
