package coindepositveri

import (
	"context"
	"time"

	"gitlab.com/tokend/coinpayments/internal/coin"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/regources"
	"gitlab.com/tokend/coinpayments/internal/horizonconnector"
)

const (
	requestStatePending int32  = 1
	taskVerifyDeposit   uint32 = 1024

	coinPaymentsTxCompleted = 1
	coinPaymentsTxPending   = 0
	coinPaymentsTxCancelled = -1
	coinPaymentsTxReceived  = 100 // FIX actually >= 100
)

type PaymentService interface {
	GetTxInfo(txid string) (*coin.TxInfo, error)
}

type issuanceRequestsStreamer interface {
	StreamIssuanceRequests(ctx context.Context, opts horizon.IssuanceRequestStreamingOpts) <-chan horizon.ReviewableRequestEvent
}

type AssetProvider interface {
	// ByCode returns nil,nil if the Asset with the AssetCode is not found.
	ByCode(code string) (*horizonconnector.Asset, error)
}

type TXSubmitter interface {
	Submit(ctx context.Context, envelope string) horizon.SubmitResult
}

// Service implements app.Service interface.
// and reviews pending issuance requests.
type Service struct {
	log          *logan.Entry
	source       keypair.Address
	signer       keypair.Full
	coinpayments PaymentService
	// TODO Interface
	builder          *xdrbuild.Builder
	assets           AssetProvider
	submitter        TXSubmitter
	issuanceStreamer issuanceRequestsStreamer

	streamingOpts horizon.IssuanceRequestStreamingOpts
}

// New is constructor for the coindepositveri Service.
//
// Make sure txsubmitter provided to constructor is with signer.
func New(
	log *logan.Entry,
	source keypair.Address,
	signer keypair.Full,
	coinpayments PaymentService,
	builder *xdrbuild.Builder,
	assets AssetProvider,
	submitter TXSubmitter,
	issuanceStreamer issuanceRequestsStreamer,
) *Service {
	return &Service{
		log:              log,
		source:           source,
		signer:           signer,
		coinpayments:     coinpayments,
		builder:          builder,
		assets:           assets,
		issuanceStreamer: issuanceStreamer,
		submitter:        submitter,
		streamingOpts: horizon.IssuanceRequestStreamingOpts{
			StopOnEmptyPage: true,
			ReverseOrder:    false,
			RequestState:    requestStatePending,
		},
	}
}

//Run is used to run coindepositveri service which will listen for
//pending issuance request and process the ones that suit
func (s *Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	running.WithBackOff(ctx, s.log, "coinpayments_deposit_verify", s.processAllIssuancesOnce, 30*time.Second, 30*time.Second, time.Hour)
}

func (s *Service) processAllIssuancesOnce(ctx context.Context) error {
	requestEvents := s.issuanceStreamer.StreamIssuanceRequests(ctx, s.streamingOpts)

	for {
		if running.IsCancelled(ctx) {
			return nil
		}

		select {
		case <-ctx.Done():
			return nil
		case event, ok := <-requestEvents:
			if !ok {
				// No more requests
				return nil
			}

			request, err := event.Unwrap()
			if err != nil {
				return errors.Wrap(err, "received error from IssuanceRequest stream")
			}

			if err := s.processRequest(ctx, request); err != nil {
				s.log.WithError(err).WithFields(logan.F{
					"request_id": request.ID,
					"request":    request,
				}).Error("Failed to process request.")
				continue
			}
		}
	}
}

func (s *Service) processRequest(ctx context.Context, request *regources.ReviewableRequest) error {
	if !s.proveCoinpayments(request) {
		return nil
	}
	txnID := s.getTXID(*request)
	if txnID == nil {
		return nil
	}

	cpTxInfo, err := s.coinpayments.GetTxInfo(*txnID)
	if err != nil {
		return errors.Wrap(err, "failed to get Coinpayments Tx info")
	}
	fields := logan.F{
		"cp_tx_info": cpTxInfo,
	}

	switch cpTxInfo.Status {
	case coinPaymentsTxCompleted, coinPaymentsTxReceived:
		if err := s.approveRequest(ctx, request); err != nil {
			return errors.Wrap(err, "failed to approve request", fields)
		}

		s.log.WithField("request_id", request.ID).Info("Successfully approved IssuanceRequest.")
		return nil
	case coinPaymentsTxPending:
		return nil
	case coinPaymentsTxCancelled:
		err = s.permanentlyRejectRequest(ctx, request, "transaction timed out")
		if err != nil {
			return errors.Wrap(err, "failed to reject request (reason: Coinpayments Tx cancelled)", fields)
		}

		s.log.WithField("request_id", request.ID).Info("Successfully permanently rejected IssuanceRequest.")
		return nil
	default:
		return errors.From(errors.New("unexpected Coinpayments Tx status"), fields)
	}
}

func (s *Service) proveCoinpayments(request *regources.ReviewableRequest) bool {
	issuanceCreate := request.Details.IssuanceCreate
	if issuanceCreate == nil {
		return false
	}

	asset, err := s.assets.ByCode(issuanceCreate.Asset)
	if err != nil {
		return false
	}
	if asset == nil {
		return false
	}

	return s.isCoinpayments(*asset)
}

func (s *Service) getTXID(request regources.ReviewableRequest) *string {
	extDetails := request.Details.IssuanceCreate.ExternalDetails

	txID, ok := extDetails["txn_id"].(string)
	if !ok {
		return nil
	}
	return &txID
}

// This is method, not a function because implementation could potentially change and a new one would require the Service to be accessible.
func (s *Service) isCoinpayments(asset horizonconnector.Asset) bool {
	return asset.Details.IsCoinpayments
}

