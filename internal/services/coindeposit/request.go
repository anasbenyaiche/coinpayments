package coindeposit

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type Request struct {
	Data RequestData `json:"data"`
}

type RequestData struct {
	Attributes RequestAttributes `json:"attributes"`
}

type RequestAttributes struct {
	Amount   string `json:"amount"`
	Receiver string `json:"receiver"`
}

func (r *Request) Validate() error {
	return validation.Errors{
		"/data/attributes/amount":   validation.Validate(&r.Data.Attributes.Amount, validation.Required, is.Float),
		"/data/attributes/receiver": validation.Validate(&r.Data.Attributes.Receiver, validation.Required),
	}.Filter()
}

func NewRequest(r *http.Request) (Request, error) {
	var request Request
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	return request, request.Validate()
}
