package coindeposit

import (
	"encoding/json"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/horizon-connector"
)

type Identity struct {
	Type       string             `json:"type"`
	ID         string             `json:"id"`
	Attributes IdentityAttributes `json:"attributes"`
}

func (i Identity) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"type":       i.Type,
		"id":         i.ID,
		"attributes": i.Attributes,
	}
}

type IdentityAttributes struct {
	Address string `json:"address"`
	Email   string `json:"email"`
}

func (i IdentityAttributes) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"address": i.Address,
		"email":   i.Email,
	}
}

type IdentityConnector struct {
	client *horizon.Client
}

func NewIdentityConnector(client *horizon.Client) *IdentityConnector {
	return &IdentityConnector{
		client: client,
	}
}

func (c *IdentityConnector) GetIdentity(address string) (*Identity, error) {
	endpoint := fmt.Sprintf("/identities?filter[address]=%s", address)
	response, err := c.client.Get(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "request failed")
	}

	if response == nil {
		return nil, nil
	}

	var result struct {
		Data []Identity `json:"data"`
	}

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal")
	}

	if len(result.Data) == 0 {
		// No Identities was found for this Address(AccountID)
		return nil, nil
	}

	return &result.Data[0], nil
}
