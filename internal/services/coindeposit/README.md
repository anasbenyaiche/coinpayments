# Coinpayments deposits

Coinpayments deposit service was created in order to simplify accepting of altcoins.
It uses [coinpayments API](https://www.coinpayments.net/apidoc) for payment processing.

In order to perform deposit using this service, one should send HTTP `POST` request to endpoint `HOST:PORT/deposit`.
HOST and PORT should be specified in config in section **coinpayment** (see config.yaml)

##Request

```json
{
    "amount": "1.2345",
    "balance": "SOME_VALID_BALANCE"
}
```

**Amount** is the sum user wants to deposit (*string*).
Amount in request can be specified with precision of asset user wants to deposit to(i.e. gwei for ETH, satoshi for BTC etc.)


**Balance** is a user balance in our system, where funds should be sent to on successful deposit completion (*string*).
Also, we get asset for tx creation from the ```balance``` in our system.  

***Note***: core has precision up to 6 digits, all the following digits will be omitted, i.e. amount will be rounded down.
**Important notice:** [Coinpayments](https://www.coinpayments.net/) sets timeouts for each transaction.
If payment was not completed during timeout, transaction is considered to be failed.

All transaction details, i.e. address to send funds to, creation time,
timeout are stored in the ExternalDetails of IssuanceRequest.

On successful Coinpayments' transaction creation and issuance request creation,
service returns http status 200 (OK).

## Response

```json
{
	"title": "Success",
	"status": 200,
	"detail": "request was successful",
	"extras": {}
}
```

Field `details` will contain error if such occurred during processing of deposit request.
In case of successful transaction creation, field `extras` will contain
the following coinpayments' response:

```json
{
   "error":"ok",
   "result":{
      "amount":"1.00000000",
      "address":"YYY",
      "txn_id":"XXX",
      "confirms_needed":"10",
      "timeout":9000,
      "status_url":"https:\/\/www.coinpayments.net\/index.php?cmd=status&id=XXX&key=ZZZ"
      "qrcode_url":"https:\/\/www.coinpayments.net\/qrgen.php?id=XXX&key=ZZZ"
   }
}
```