package coindeposit

type Response struct {
	Data ResponseData `json:"data"`
}

type ResponseData struct {
	Type       string
	ID         string
	Attributes ResponseAttributes `json:"attributes"`
}

type ResponseAttributes struct {
	Amount         string `json:"amount"`
	Address        string `json:"address"`
	ConfirmsNeeded string `json:"confirms_needed"`
	Timeout        int32  `json:"timeout"`
}
