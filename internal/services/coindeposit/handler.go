package coindeposit

import (
	"net/http"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func (s *Service) prepareRouter() chi.Router {
	router := chi.NewRouter()

	router.Post("/integrations/coinpayments/deposit", s.depositHandler)

	if err := s.cop.RegisterChi(router); err != nil {
		panic(errors.Wrap(err, "failed to register in cop"))
	}

	return router
}

func (s *Service) depositHandler(w http.ResponseWriter, r *http.Request) {
	depositRequest, err := NewRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	fields := logan.F{
		"deposit_request": depositRequest,
	}
	s.log.WithFields(fields).Info("Received request.")

	identity, err := s.getIdentity(depositRequest.Data.Attributes.Receiver)
	if err != nil {
		s.log.WithError(err).WithFields(fields).Error("Failed to get Identity.")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if identity == nil {
		ape.RenderErr(w, problems.BadRequest(validation.Errors{
			"/data/attributes/receiver": errors.New("does not exist"),
		})...)
		return
	}
	fields = fields.Merge(logan.F{
		"identity": identity,
	})

	assetCode, err := s.getCoinpaymentsAssetCode(identity.Attributes.Address, depositRequest.Data.Attributes.Receiver)
	if err != nil {
		s.log.WithError(err).WithFields(fields).Error("Failed to get and check Asset.")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if assetCode == nil {
		ape.RenderErr(w, problems.BadRequest(validation.Errors{
			"/data/attributes/receiver": errors.New("asset could not be processed with Coinpayments"),
		})...)
		return
	}
	fields = fields.Merge(logan.F{
		"asset_code": *assetCode,
	})

	cpTxDetails, err := s.createCoinpaymentsTx(depositRequest.Data.Attributes, *assetCode, identity.Attributes.Email)
	if err != nil {
		s.log.WithError(err).WithFields(fields).Error("Failed to create Coinpayments Tx.")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	fields = fields.Merge(logan.F{
		"cp_tx_details": cpTxDetails,
	})

	if err = s.processIssuance(r.Context(), depositRequest.Data.Attributes, *assetCode, cpTxDetails); err != nil {
		s.log.WithError(err).WithFields(fields).Error("Failed to process Issuance.")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	s.log.WithFields(fields).Info("CreateIssuanceRequest was submitted successfully.")

	ape.Render(w, Response{
		Data: ResponseData{
			Type: "coinpayments_transaction",
			ID:   cpTxDetails.TxnID,
			Attributes: ResponseAttributes{
				Amount:         cpTxDetails.Amount,
				Address:        cpTxDetails.Address,
				ConfirmsNeeded: cpTxDetails.ConfirmsNeeded,
				Timeout:        cpTxDetails.Timeout,
			},
		},
	})
}
