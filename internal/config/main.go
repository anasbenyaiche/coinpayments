package config

import (
	_ "github.com/lib/pq"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/copus"
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/coinpayments/internal/coin"
)

var CoinpaymentsRelease string

type Config interface {
	comfig.Listenerer
	comfig.Logger
	types.Copuser
	Horizoner
	coin.Coiner
	DepositConfig() DepositConfig
	DepositVerifyConfig() DepositVerifyConfig
	WithdrawConfig() WithdrawConfig
	AtomicSwapChecker() AtomicSwapCheckerConfig
	AtomicSwapMatcher() AtomicSwapMatcherConfig
	AtomicSwap() AtomicSwapConfig
}

type config struct {
	comfig.Listenerer
	comfig.Logger
	types.Copuser
	Horizoner
	coin.Coiner

	getter kv.Getter

	deposit           comfig.Once
	depositVerify     comfig.Once
	withdraw          comfig.Once
	atomicSwapChecker comfig.Once
	atomicSwapMatcher comfig.Once
}

func NewConfig(getter kv.Getter) Config {
	return &config{
		getter:     getter,
		Listenerer: comfig.NewListenerer(getter),
		Logger:     comfig.NewLogger(getter, comfig.LoggerOpts{Release: CoinpaymentsRelease}),
		Horizoner:  NewHorizoner(getter),
		Coiner:     coin.NewCoiner(getter),
		Copuser:    copus.NewCopuser(getter),
	}
}
