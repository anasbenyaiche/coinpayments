package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type DepositConfig struct {
	Source keypair.Address `fig:"source,required"`
	Signer keypair.Full    `fig:"signer,required"`
}

func (c *config) DepositConfig() DepositConfig {
	return c.deposit.Do(func() interface{} {
		var result DepositConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "deposit")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out deposit"))
		}

		return result
	}).(DepositConfig)
}
