package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type WithdrawConfig struct {
	Source      keypair.Address `fig:"source,required"`
	Signer      keypair.Full    `fig:"signer,required"`
	AddTxFee    bool            `fig:"add_tx_fee,required"`
	AutoConfirm bool            `fig:"auto_confirm,required"`
}

func (c *config) WithdrawConfig() WithdrawConfig {
	return c.withdraw.Do(func() interface{} {
		var result WithdrawConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "withdraw")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out withdraw"))
		}
		return result
	}).(WithdrawConfig)
}
