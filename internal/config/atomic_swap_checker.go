package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type AtomicSwapCheckerConfig struct {
	Signer keypair.Full `fig:"signer,required"`
}

func (c *config) AtomicSwapChecker() AtomicSwapCheckerConfig {
	return c.atomicSwapChecker.Do(func() interface{} {
		var result AtomicSwapCheckerConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "atomic_swap_checker")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out atomic_swap_checker"))
		}

		return result
	}).(AtomicSwapCheckerConfig)
}
