package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type AtomicSwapMatcherConfig struct {
	Signer         keypair.Full `fig:"signer,required"`
	ExcludedAssets []string     `fig:"excluded_assets"`
}

func (c *config) AtomicSwapMatcher() AtomicSwapMatcherConfig {
	return c.atomicSwapMatcher.Do(func() interface{} {
		var result AtomicSwapMatcherConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "atomic_swap_matcher")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out atomic_swap_matcher"))
		}

		return result
	}).(AtomicSwapMatcherConfig)
}
