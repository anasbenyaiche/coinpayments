package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type AtomicSwapConfig struct {
	Signer          keypair.Full `fig:"signer,required"`
	PriceAsset      string       `fig:"price_asset,required"`
	ForbillEndpoint string       `fig:"forbill_endpoint,required"`
}

func (c *config) AtomicSwap() AtomicSwapConfig {
	return c.atomicSwapChecker.Do(func() interface{} {
		var result AtomicSwapConfig

		err := figure.
			Out(&result).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "atomic_swap")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out atomic_swap"))
		}

		return result
	}).(AtomicSwapConfig)
}
