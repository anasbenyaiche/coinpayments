package cli

import (
	"context"

	"gitlab.com/tokend/coinpayments/internal/forbill"

	"gitlab.com/tokend/coinpayments/internal/services/atomicswap"

	"gitlab.com/tokend/coinpayments/internal/services/atomicswapmatcher"

	"gitlab.com/tokend/coinpayments/internal/data"

	"gitlab.com/tokend/coinpayments/internal/services/atomicswapchecker"

	"fmt"

	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/coinpayments/internal/config"
	"gitlab.com/tokend/coinpayments/internal/horizonconnector"
	"gitlab.com/tokend/coinpayments/internal/services/coindeposit"
	"gitlab.com/tokend/coinpayments/internal/services/coindepositveri"
	"gitlab.com/tokend/coinpayments/internal/services/coinwithdraw"
)

func Run(args []string) bool {
	// TODO consider kingpin
	var cfg config.Config
	log := logan.New()

	defer func() {
		if rvr := recover(); rvr != nil {
			log.WithRecover(rvr).Error("app panicked")
		}
	}()

	app := cli.NewApp()

	before := func(_ *cli.Context) error {
		getter, err := kv.FromEnv()
		if err != nil {
			if err == kv.ErrNoBackends {
				fmt.Println("Could not get config - is KV_VIPER_FILE env var provided?")
			}
			return errors.Wrap(err, "failed to get config")
		}

		cfg = config.NewConfig(getter)
		log = cfg.Log()
		return nil
	}

	app.Commands = cli.Commands{
		{
			Name: "run",
			Subcommands: cli.Commands{
				{
					Name:   "deposit",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := coindeposit.New(
							cfg.Log(),
							cfg.Listener(),
							cfg.Copus(),
							cfg.DepositConfig().Source,
							cfg.DepositConfig().Signer,
							builder,
							cfg.Horizon(),
							cfg.Coin(),
						)

						service.Run(context.Background())
						return errors.New("coindeposit service died")
					},
				},
				{
					Name:   "deposit-verify",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := coindepositveri.New(
							cfg.Log(),
							cfg.DepositVerifyConfig().Source,
							cfg.DepositVerifyConfig().Signer,
							cfg.Coin(),
							builder,
							horizonconnector.NewAssetConnector(cfg.Horizon().Client()),
							cfg.Horizon().Submitter(),
							cfg.Horizon().Listener(),
						)

						service.Run(context.Background())
						return errors.New("coindepositveri service died")
					},
				},
				{
					Name:   "withdraw",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := coinwithdraw.New(
							cfg.Log(),
							cfg.WithdrawConfig().Source,
							cfg.WithdrawConfig().Signer,
							cfg.WithdrawConfig().AddTxFee,
							cfg.WithdrawConfig().AutoConfirm,
							cfg.Horizon().Listener(),
							cfg.Horizon().Operations(),
							cfg.Horizon().Submitter(),
							builder,
							cfg.Coin(),
							horizonconnector.NewAssetConnector(cfg.Horizon().Client()),
							cfg.Horizon().Balances(),
						)

						service.Run(context.Background())
						return errors.New("coinwithdraw service died")
					},
				},
				{
					Name:   "atomic-swap-checker",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := atomicswapchecker.New(
							cfg.Log(),
							data.NewV3ReviewableRequestStreamer(cfg.Horizon()).WithState(data.FilterStatePending),
							cfg.AtomicSwapChecker().Signer,
							data.NewAtomicSwapConnector(cfg.Horizon().Client()),
							builder,
							data.NewIdentityConnector(cfg.Horizon().Client()),
							cfg.Coin(),
							cfg.Horizon(),
						)
						service.Run(context.Background())
						return errors.New("atomic-swap-checker service died")
					},
				},
				{
					Name:   "atomic-swap-matcher",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := atomicswapmatcher.New(
							cfg.Log(),
							data.NewV3ReviewableRequestStreamer(cfg.Horizon()).WithState(data.FilterStatePending).WithPendingTasks(data.TaskPendingForTx),
							cfg.AtomicSwapMatcher().Signer,
							builder,
							data.NewIdentityConnector(cfg.Horizon().Client()),
							cfg.Coin(),
							cfg.Horizon(),
							cfg.AtomicSwapMatcher().ExcludedAssets,
						)

						service.Run(context.Background())
						return errors.New("atomic-swap-matcher service died")
					},
				},
				{
					Name:   "atomic-swap",
					Before: before,
					Action: func(_ *cli.Context) error {
						builder, err := cfg.Horizon().TXBuilder()
						if err != nil {
							return errors.Wrap(err, "failed to prepare TXBuilder")
						}

						service := atomicswap.New(
							cfg.Log(),
							cfg.Listener(),
							cfg.Copus(),
							cfg.AtomicSwap().Signer,
							builder,
							cfg.Horizon(),
							cfg.Coin(),
							data.NewAtomicSwapConnector(cfg.Horizon().Client()),
							data.NewIdentityConnector(cfg.Horizon().Client()),
							cfg.AtomicSwap().PriceAsset,
							forbill.NewConnector(cfg.AtomicSwap().ForbillEndpoint),
						)

						service.Run(context.Background())
						return errors.New("coinwithdraw service died")
					},
				},
			},
		},
	}

	if err := app.Run(args); err != nil {
		log.WithError(err).Error("app failed")
		return false
	}
	return true
}
