package forbill

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3/errors"

	"github.com/google/jsonapi"
)

type Connector struct {
	domain string
	client *http.Client
}

func NewConnector(domain string) *Connector {
	return &Connector{
		domain: domain,
		client: http.DefaultClient,
	}
}

func (c *Connector) Pay(txEnvelope string, xForwardedFor string) (*Success, *jsonapi.ErrorsPayload, error) {
	url := fmt.Sprintf("%s/integrations/fiat/pay", c.domain)

	body, err := json.Marshal(PayRequestBody{
		Tx: txEnvelope,
	})
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to marshal request body to json")
	}
	rb := bytes.NewReader(body)
	req, err := http.NewRequest("POST", url, rb)
	req.Header.Add("x-forwarded-for", xForwardedFor)
	req.Header.Add("Content-Type", "application/vnd.api+json")

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to send message")
	}

	defer resp.Body.Close()

	bodyBB, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to read response body")
	}

	if isSuccess(*resp) {
		var result Success
		if err = json.Unmarshal(bodyBB, &result); err != nil {
			return nil, nil, errors.Wrap(err, "failed to unmarshal response body")
		}
		return &result, nil, nil
	} else {
		var result jsonapi.ErrorsPayload
		if err = json.Unmarshal(bodyBB, &result); err != nil {
			return nil, nil, errors.Wrap(err, "failed to unmarshal response body")
		}
		return nil, &result, nil
	}
}

func isSuccess(resp http.Response) bool {
	return resp.StatusCode >= 200 && resp.StatusCode < 300
}
