package forbill

type PayRequestBody struct {
	Tx string `json:"tx"`
}

type Success struct {
	Data SuccessData `json:"data"`
}

type SuccessData struct {
	Attributes SuccessDataAttributes `json:"attributes"`
}

type SuccessDataAttributes struct {
	PayURL string `json:"pay_url"`
}
