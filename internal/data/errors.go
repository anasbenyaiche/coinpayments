package data

var (
	ErrInvalidRequest         = Error{"request was invalid in some way"}
	ErrRelatedAskNotFound     = Error{"related ask request not found"}
	ErrNoAddresses            = Error{"addresses not found"}
	ErrIdentityNotFound       = Error{"identity not found"}
	ErrInvalidAmount          = Error{"invalid amount"}
	ErrCoinpaymentTransaction = Error{"coinpayments transaction failed"}
	ErrFailedToCreateInvoice  = Error{"failed to create invoice"}
	ErrInternalProblems       = Error{"internal error"}
	ErrCanceledRequest        = Error{"request was canceled"}
)

type Error struct {
	msg string
}

func (e Error) Error() string {
	return e.msg
}
