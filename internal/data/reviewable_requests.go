package data

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/distributed_lab/logan"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/horizon-connector"
	regources "gitlab.com/tokend/regources/generated"
)

const (
	FilterStatePending             = 1
	FilterStateCanceled            = 2
	FilterStateApproved            = 3
	FilterStateRejected            = 4
	FilterStatePermanentlyRejected = 5
)

const TaskPendingForTx = 2 ^ 10

var ErrEmptyPage = errors.New("empty page")

type ReviewableRequestStreamer struct {
	connector *horizon.Connector

	reviewableRequestFilters
	currentPage *regources.ReviewableRequestListResponse

	reviewableRequestStream chan regources.ReviewableRequestListResponse
	errs                    chan error
}

type reviewableRequestFilters struct {
	state             *[]int
	pendingTasksAnyOf *[]int
	pendingTasksNotSet *[]int
}

func NewV3ReviewableRequestStreamer(connector *horizon.Connector) *ReviewableRequestStreamer {
	return &ReviewableRequestStreamer{
		connector:               connector,
		reviewableRequestStream: make(chan regources.ReviewableRequestListResponse),
		errs:                    make(chan error),
	}
}

func (ts *ReviewableRequestStreamer) WithState(state ...int) *ReviewableRequestStreamer {
	ts.reviewableRequestFilters.state = &state
	return ts
}

func (ts *ReviewableRequestStreamer) WithPendingTasks(tasks ...int) *ReviewableRequestStreamer {
	ts.reviewableRequestFilters.pendingTasksAnyOf = &tasks
	return ts
}

func (ts *ReviewableRequestStreamer) WithPendingTasksNotSet(tasks ...int) *ReviewableRequestStreamer {
	ts.reviewableRequestFilters.pendingTasksNotSet = &tasks
	return ts
}

func (ts *ReviewableRequestStreamer) New() *ReviewableRequestStreamer {
	return &ReviewableRequestStreamer{
		connector:               ts.connector,
		reviewableRequestStream: make(chan regources.ReviewableRequestListResponse),
		errs:                    make(chan error),
	}
}

func (f *reviewableRequestFilters) String() string {
	u := url.Values{}

	if f.state != nil {
		u.Add("filter[state]", filters(*f.state))
	}

	if f.pendingTasksAnyOf != nil {
		u.Add("filter[pending_tasks_any_of]", filters(*f.pendingTasksAnyOf))
	}

	if f.pendingTasksNotSet != nil {
		u.Add("filter[pending_tasks_not_set]", filters(*f.pendingTasksNotSet))
	}

	u.Add("include", "request_details")

	return u.Encode()
}

func filters(values []int) string {
	var s []string
	for _, v := range values {
		s = append(s, strconv.Itoa(v))
	}

	return strings.Join(s, ",")
}

func (ts *ReviewableRequestStreamer) buildQuery() string {
	return fmt.Sprintf("/v3/create_atomic_swap_bid_requests?%s", ts.reviewableRequestFilters.String())
}

func (ts *ReviewableRequestStreamer) Errors() <-chan error {
	return ts.errs
}

func (ts *ReviewableRequestStreamer) Stream() <-chan regources.ReviewableRequestListResponse {
	go func() {
		defer func() {
			if rvr := recover(); rvr != nil {
				ts.errs <- errors.Wrap(errors.FromPanic(rvr), "streaming panicked")
			}
		}()

		tick := time.NewTicker(10 * time.Second)
		for {
			page, err := ts.Next()
			if err != nil && err != ErrEmptyPage {
				ts.errs <- err
				return
			}

			if page == nil {
				<-tick.C // if no requests wait a bit
				continue
			}

			ts.reviewableRequestStream <- *page

		}
	}()

	return ts.reviewableRequestStream
}

func (ts *ReviewableRequestStreamer) Next() (next *regources.ReviewableRequestListResponse, err error) {
	if ts.currentPage == nil {
		firstPage, err := ts.retrievePage(ts.buildQuery())
		if err != nil {
			return nil, errors.Wrap(err, "cannot retrieve first page")
		}

		if len(firstPage.Data) == 0 {
			return nil, ErrEmptyPage
		}
		ts.currentPage = firstPage
		return ts.currentPage, nil
	}

	next, err = ts.retrievePage(ts.currentPage.Links.Next)
	if err != nil {
		return nil, errors.Wrap(err, "cannot retrieve page")
	}

	if len(next.Data) == 0 {
		return nil, nil
	}

	ts.currentPage = next

	return ts.currentPage, nil
}

func (ts *ReviewableRequestStreamer) retrievePage(query string) (*regources.ReviewableRequestListResponse, error) {
	responseBB, err := ts.connector.Client().Get(query)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get page from horizon", logan.F{
			"query": query,
		})
	}

	if responseBB == nil {
		return nil, errors.New("retrieved empty page")
	}

	var t regources.ReviewableRequestListResponse
	err = json.Unmarshal(responseBB, &t)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode response bytes")
	}

	return &t, nil
}
