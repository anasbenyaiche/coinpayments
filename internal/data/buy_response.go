package data

type BuyResponse struct {
	Data BuyResponseData `json:"data"`
}

type BuyResponseData struct {
	ID         string                `json:"id"`
	Type       string                `json:"type"`
	Attributes BuyResponseAttributes `json:"attributes"`
}

type BuyResponseAttributes struct {
	Type BuyResponseType `json:"type"`
	Data interface{}     `json:"data"`
}

type BuyResponseType string

const (
	BuyResponseTypeCrypto   BuyResponseType = "crypto_invoice"
	BuyResponseTypeRedirect                 = "redirect"
)

type FiatBuyResponse struct {
	PayUrl string `json:"pay_url"`
}
